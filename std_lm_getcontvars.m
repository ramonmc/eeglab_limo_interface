% std_lm_getcontvars() -Retrieve covariate from a design in the
% STUDY structure to build the regressors
%
% Usage:
%   >>  [catvar,varnames,StartEndIndx] =
%   std_lm_getcatvars(STUDY,ALLEEG,'S01','design_indx',1);
%
% Inputs:
%      STUDY       - studyset structure containing some or all files in ALLEEG
%      ALLEEG      - vector of loaded EEG datasets
%      subject     - String with the subject identifier
%      design_indx - Index of the design in he STUDY structure
%
% Optional inputs:
%
%
% contvfield    - Cell array of strings. Field in STUDY.datasetinfo.trialinfo to be used as covariate.  
% setindx       - Index of subjects to include in the parsing. This
%                 functionality is not designed to be used within the std_lm_setdesign
%                 streamline, but for to be used when the function is called as standalone
%                 function from command line. By default within the
%                 std_lm_setdesign streamline  it will use ALL the subjects
%                 under the variable subject
%
%
% Outputs:
% contvar      - Matrix with dimension = (Nb of trials,Nb of fields provided). Covariates retrieved from the fields provided
%
% See also:
%   std_lm_setdesign , std_lm_getcontvars
%
% Author: Ramon Martinez-Cancino, SCCN, 2014
%
% Copyright (C) 2014  Ramon Martinez-Cancino,INC, SCCN
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [contvar_matrix,contvar_info] = std_lm_getcontvars(STUDY,ALLEEG,subject,varargin)

% Prevent empty output 
contvar_matrix = [];
% varname = [];

%% Varargin stuff
%..........................................................................
try
    options = varargin;
    if ~isempty( varargin ),
        for i = 1:2:numel(options)
            g.(options{i}) = options{i+1};
        end
    else g= []; end;
catch
    error('std_checkdatasession() error: calling convention {''key'', value, ... } error');
end;

try, g.design;              catch, g.design        = 1;        end; % By default will be use the first design if not especified
try, g.setindx;             catch, g.setindx       = '';       end; % if not provided it will look for all the Sets that belong to this Subject
try, g.contvfield;          catch, g.contvfield    = '';       end; % Field of datasetinfo.trialfo who contain the cont variable (priority over custom)
try, g.factors;             catch, g.factors       = '';       end; % If not provided it will use all the factors
try, g.nanmat;              catch, g.nanmat        = 0;        end; % If all factors are not provided, it wil fill the catvar_matrix with zeros (1) or NaNs (0)

% try, g.contvcustom;         catch, g.contvcustom   = '';       end; % Custom continous variable
% try, g.varname;             catch, g.varname       = '';       end; % Name for custom continous variables

%% Checking setindex and valid subject
%..........................................................................
CurrentSubIndxDesign    = find(strcmp({STUDY.design(g.design).cell.case},subject)); % former CurrentSubIndx

 if isempty(subject)
     error('std_lm_getcatvars() error: A valid subject must be provided');
 end
 
CurrentSubIndxDataset = [STUDY.design(g.design).cell(CurrentSubIndxDesign).dataset];

if ~isempty(g.setindx)
    if sum(ismember(CurrentSubIndxDataset, g.setindx)) == 0
        error('std_lm_getcatvars() error: Indices of sets does not match the subject provided ');
    end
    
    for i = 1 length(g.setindx)
        g.setIndxDesign(i) = find(ismember([STUDY.design(g.design).cell.dataset], g.setindx(i)));
    end
    
else
    g.setIndxDesign = CurrentSubIndxDesign;
    g.setindx       = CurrentSubIndxDataset;
end

%% Checking contvfield and contvcustom
if isempty(g.contvfield) %& isempty(g.contvcustom)
    error('std_checkdatasession() error: contvfield option must be provided');
end


%% Getting Number of trials
%..........................................................................
NbTrials   = 0;

for i = 1 : length(g.setIndxDesign)
    NbTrials = NbTrials + length(STUDY.design(g.design).cell(g.setIndxDesign(i)).trials{:});
end

%% Getting factors
%..........................................................................
if isempty(g.factors)
    g.factors  =  {STUDY.design(g.design).variable.value};
    
    % Cleaning  'g.factors' from empty cells
    for i = 1 : length(g.factors)
        if isempty(g.factors{i}) | strcmp(g.factors{i},''), g.factors(i) = []; end;
    end
else
    % Place to check consitency of input
end

%% Number of trials and index
%..........................................................................
NbTrials   = 0;

TrialIndx_datasetinfo = '';
 
for i = 1 : length(g.setIndxDesign)
    NbTrials = NbTrials + length(STUDY.design(g.design).cell(g.setIndxDesign(i)).trials{:});
    
    % ---
    if i == 1,
        StartEndIndx{i} = 1: length(STUDY.design(g.design).cell(g.setIndxDesign(i)).trials{:});
    else
        StartEndIndx{i} = StartEndIndx{i-1}(end) +1 : StartEndIndx{i-1}(end) + length(STUDY.design(g.design).cell(g.setIndxDesign(i)).trials{:});
    end
    
    for j = 1 : length(g.factors)
        catvar_names{i,j}   =  STUDY.design(g.design).cell(g.setIndxDesign(i)).value(j);
    end
    
    TrialIndx_datasetinfo{i} =  STUDY.design(g.design).cell(g.setIndxDesign(i)).trials{:};
    dataset{i}               =  STUDY.design(g.design).cell(g.setIndxDesign(i)).dataset;
end

%% Getting covariates from STUDY design
%..........................................................................
if ~isempty(g.contvfield)
    
    if ~iscell(g.contvfield)
        tmp = g.contvfield;
        g.contvfield = cellstr(tmp);
    end
    
    % Check if trialinfo field
    %......................................................................
    if  ~isfield(STUDY.datasetinfo(g.setindx), 'trialinfo')
        error('std_checkdatasession() error: must provide a valid field from STUDY');
    end
    
    % Check consistency of field requested in trialinfo
    %......................................................................
    for i = length(g.contvfield)
        fieldinstr = sum(cellfun(@(x) isfield(x, g.contvfield(i)), {STUDY.datasetinfo(g.setindx).trialinfo}));
        if fieldinstr ~=  length(STUDY.datasetinfo((g.setindx)))
            error(['std_checkdatasession() error: Field ' ''''  char(g.contvfield(i)) '''' ' is not a valid field or is not consistent across subjects']);
        end
    end
    
    %
    %......................................................................
    contvar_matrix = zeros(NbTrials,length(g.contvfield));
    
    for i = 1 : length(g.setindx)
        for j = 1 : length(g.contvfield)
            
            tmpfield = char(cellstr(g.contvfield(j)));
            tmpmat   = [STUDY.datasetinfo(g.setindx(i)).trialinfo.(tmpfield)];
            contvar_matrix(StartEndIndx{i},j) = tmpmat(TrialIndx_datasetinfo{i});
        end
    end
end


%LIMO STUFF
if g.nanmat
    tmp_dat = unique(CurrentSubIndxDataset,'stable');
    
    % getting dataset dimension
    datasets_lengths(1) = 0;
    for i = 1: length(tmp_dat)
%         tmpindx_datasetinfo = STUDY.design(g.design).cell(tmp_dat(i)).dataset;
        tmpindx_ALLEEG      = STUDY.datasetinfo(tmp_dat(i)).index ;
        datasets_lengths(i+1) = ALLEEG(tmpindx_ALLEEG).trials;
    end
    set_positions = cumsum(datasets_lengths);
    
    contvar_nan = nan(set_positions(end),length(g.contvfield));
    
    start_contvar_indx = 1;
    for i = 1: length(TrialIndx_datasetinfo)
        
        end_contvar_indx = start_contvar_indx + length(TrialIndx_datasetinfo{i})-1;
        
        
        dataset_nb = find(dataset{i} == tmp_dat);
        contvar_nan(TrialIndx_datasetinfo{i} + set_positions(dataset_nb),:) =  contvar_matrix(start_contvar_indx:end_contvar_indx,:);
        
        start_contvar_indx = end_contvar_indx + 1;
    end
    
    % Reasigning result
    contvar_matrix = contvar_nan;
end


contvar_info.datasetinfo_trialindx = TrialIndx_datasetinfo ;
contvar_info.dataset               = [dataset{:}];
contvar_info.designmat_trialindx   = StartEndIndx;
contvar_info.names                 = catvar_names;
contvar_info.fieldnames            = g.contvfield;
