function LIMO_files = std_to_limo(STUDY,ALLEEG,Analysis,design_index,cont_var_fields)

%--- DEPRECATED --

% FORMAT std_to_limo(STUDY)
% INPUT STUDY is the EEGLAB STUDY structure
%       ALLEEG is the set of EEG.set of the study
%       Analysis is 'daterp', 'icaerp', 'datspec', 'icaspec', 'datersp', 'icaersp'
%       design_index this is the indice of the design in the study
%       cont_var_fields are the fields of EEG.event to use as continuous
%       variable - if none use []
% OUTPUT call limo_batch to create all 1st level LIMO_EEG analysis + RFX
%             LIMO_files a structure with the following fields
%             LIMO_files.LIMO the LIMO folder name where the study is analyzed
%             LIMO_files.mat a list of 1st level LIMO.mat (with path)
%             LIMO_files.Beta a list of 1st level Betas.mat (with path)
%             LIMO_files.con a list of 1st level con.mat (with path)
%
% Example: Analysis = 'datspec'; design_index = 3; cont_var_fields = [];
%          LIMO_files = std_to_limo(STUDY,ALLEEG,Analysis,design_index,cont_var_fields )
%
% NEW Features from EEGLAB STUDY - when choosing precompute, click to save
%              all trials ; this generate a single mat file for all trials
%              (at the moment it generates multiple files per condition and
%              we merge them using std_getmeasure)
% [STUDY ALLEEG] = std_precomp(STUDY, ALLEEG, {},'rmicacomps','on','savetrials','on', ...
%    'interp','on','recompute','on','erp','on','erpparams',{'rmbase' [-10 1000] }, ....
%    'spec','on','specparams',{'specmode' 'fft' 'logtrials' 'off'});
%              Information about these files is strored in EEG.etc
%              EEG.etc.singletrials.daterp = [the big file name]
%              EEG.etc.singletrials.icaerp = [the big file name]
%              EEG.etc.singletrials.datspec = [the big file name]
%              EEG.etc.singletrials.icaspec = [the big file name]
%              EEG.etc.singletrials.datersp = [the big file name]
%              EEG.etc.singletrials.icaersp = [the big file name]
%
% Author: Cyril Pernet (LIMO Team), The university of Edinburgh, 2014
%         Ramon Martinez-Cancino, SCCN, 2014
%
% Copyright (C) 2014  Ramon Martinez-Cancino,INC, SCCN

%% 1st level analysis

model.cat_files = [];
model.cont_files = [];
if isempty(STUDY.design(design_index).filepath)
    STUDY.design(design_index).filepath = STUDY.filepath;
end

nb_subjects = length(unique({STUDY.datasetinfo.subject}));
order = unique([STUDY.design(design_index).cell.dataset],'stable');
for s = 1:nb_subjects
    [~,catvar_info] = std_lm_getcatvars(STUDY,ALLEEG,STUDY.datasetinfo(order(s)).subject,'design',design_index,'nanmat',1);
    nb_sets(s) = length(unique(catvar_info.dataset));
end
% check that all subjects use the same sets (should not happen)
if length(unique(nb_sets)) ~= 1
    error('different numbers of datasets (.set) used across subjects - cannot go further')
else
    nb_sets = unique(nb_sets);
end

% simply reshape to read columns
order = reshape(order,[length(order)/nb_sets,nb_sets])';

% what type of analysis
if strncmp(Analysis,'dat',3)
    model.type = 'Channels';
elseif strncmp(Analysis,'ica',3)
    [STUDY,flags]=std_checkdatasession(STUDY,ALLEEG);
    if sum(flags)>0
        error('some subjects have data from different sessions - can''t do ICA');
    end
    model.type = 'Components';
    model.defaults.icaclustering = 1;
end

for s = 1:nb_subjects
    % model.set_files: a cell array of EEG.set (full path) for the different subjects
    if nb_sets == 1;
        index = STUDY.datasetinfo(order(s)).index;
        names{s} = STUDY.datasetinfo(order(s)).subject;
        model.set_files{s} = [ALLEEG(index).filepath filesep ALLEEG(index).filename];
    else
        index = [STUDY.datasetinfo(order(:,s)).index];
        tmp = {STUDY.datasetinfo(order(:,s)).subject};
        if length(unique(tmp)) ~= 1
            error('it seems that sets of different subjects are merged')
        else
            names{s} =  cell2mat(unique(tmp));
        end
        
        model.set_files{s} = [ALLEEG(index(1)).filepath filesep ['merged_datasets_design' num2str(design_index) '.set']];
        if exist(model.set_files{s},'file') == 0 % && donot_update = 1
            % merge sets
            OUTEEG = pop_mergeset(ALLEEG,index,1);
            OUTEEG.filename = ['merged_datasets_design' num2str(design_index) '.set'];
            OUTEEG.datfile = [];
            % update EEG.etc
            for ns = 1:nb_sets
                OUTEEG.etc.merged{ns} = ALLEEG(index(ns)).filename;
                if ns == 1
                    OUTEEG.etc.datafiles.daterp   = [];
                    OUTEEG.etc.datafiles.datspec  = [];
                    OUTEEG.etc.datafiles.dattimef = [];
                    OUTEEG.etc.datafiles.datitc   = [];
                    OUTEEG.etc.datafiles.icaerp   = [];
                    OUTEEG.etc.datafiles.icaspec  = [];
                    OUTEEG.etc.datafiles.icatimef = [];
                    OUTEEG.etc.datafiles.icaitc   = [];
                end
                
                if isfield(ALLEEG(index(ns)).etc.datafiles,'daterp')
                    OUTEEG.etc.datafiles.daterp{ns} = ALLEEG(index(ns)).etc.datafiles.daterp;
                end
                if isfield(ALLEEG(index(ns)).etc.datafiles,'datspec')
                    OUTEEG.etc.datafiles.datspec{ns} = ALLEEG(index(ns)).etc.datafiles.datspec;
                end
                if isfield(ALLEEG(index(ns)).etc.datafiles,'dattimef')
                    OUTEEG.etc.datafiles.datersp{ns} = ALLEEG(index(ns)).etc.datafiles.dattimef;
                end
                if isfield(ALLEEG(index(ns)).etc.datafiles,'datitc')
                    OUTEEG.etc.datafiles.datitc{ns} = ALLEEG(index(ns)).etc.datafiles.datitc;
                end
                if isfield(ALLEEG(index(ns)).etc.datafiles,'icaerp')
                    OUTEEG.etc.datafiles.icaerp{ns} = ALLEEG(index(ns)).etc.datafiles.icaerp;
                end
                if isfield(ALLEEG(index(ns)).etc.datafiles,'icaspec')
                    OUTEEG.etc.datafiles.icaspec{ns} = ALLEEG(index(ns)).etc.datafiles.icaspec;
                end
                if isfield(ALLEEG(index(ns)).etc.datafiles,'icatimef')
                    OUTEEG.etc.datafiles.icaersp{ns} = ALLEEG(index(ns)).etc.datafiles.icatimef;
                end
                if isfield(ALLEEG(index(ns)).etc.datafiles,'icaitc')
                    OUTEEG.etc.datafiles.icaitc{ns} = ALLEEG(index(ns)).etc.datafiles.icaitc;
                end
            end
            % save info
            EEG = pop_saveset(OUTEEG, 'filename', OUTEEG.filename, 'filepath',OUTEEG.filepath,'savemode' ,'twofiles');
            clear OUTEEG
        else
            fprintf('using the already merged file %s \n',model.set_files{s});
            % EEG = pop_loadset(model.set_files{s});
        end
    end
    
    % model.cat_files: a cell array of categorial variable
    if nb_sets == 1;
        [catvar_matrix,catvar_info] = std_lm_getcatvars(STUDY,ALLEEG,STUDY.datasetinfo(order(s)).subject,'design',design_index,'nanmat',1);
    else
        [catvar_matrix,catvar_info] = std_lm_getcatvars(STUDY,ALLEEG,STUDY.datasetinfo(order(1,s)).subject,'design',design_index,'nanmat',1);
    end
    
    if ~isempty(catvar_matrix)
        if isvector(catvar_matrix) % only one factor of n conditions
            categ = NaN(size(catvar_matrix,1),1);
            for cond = 1:size(catvar_info.datasetinfo_trialindx,2)
                categ(cell2mat(catvar_info.datasetinfo_trialindx(cond))) = cond;
            end
            model.cat_files{s} = categ;
        elseif 1 % multiple factors (=multiple columns)

            categ = std_builddesignmat(STUDY.design(design_index), STUDY.datasetinfo(s).trialinfo);

        else % WARNING CODE NEVER REACHED *********
            X = []; nb_row = size(catvar_matrix,1);
            for cond = 1:size(catvar_matrix,2)
                nb_col(cond) = max(unique(catvar_matrix(:,cond)));
                x = NaN(nb_row,nb_col(cond));
                for c=1:nb_col(cond)
                    x(:,c) = [catvar_matrix(:,cond) == c];
                end
                X = [X x];
            end
            
            tmpX = limo_make_interactions(X, nb_col);
            tmpX = tmpX(:,sum(nb_col)+1:end);
            getnan = find(isnan(catvar_matrix(:,1)));
            if ~isempty(getnan)
                tmpX(getnan,:) = NaN;
            end
            categ = sum(repmat([1:size(tmpX,2)],nb_row,1).*tmpX,2);
            clear x X tmpX; model.cat_files{s} = categ;
        end
        save([ALLEEG(index(1)).filepath filesep 'categorical_variable.txt'],'categ','-ascii')
    end
    
    % model.cont_files: a cell array of continuous variable files
    if ~isempty(cont_var_fields)
        if nb_sets == 1;
            [contvar_matrix,contvar_info] = std_lm_getcontvars(STUDY,ALLEEG,STUDY.datasetinfo(order(s)).subject,'design',design_index,'contvfield',cont_var_fields,'nanmat',1);
            contvar = NaN(size(contvar_matrix));
            for cond = 1:size(contvar_info.datasetinfo_trialindx,2)
                values = contvar_matrix(cell2mat(contvar_info.datasetinfo_trialindx(cond)));
                contvar(cell2mat(contvar_info.datasetinfo_trialindx(cond))) = values;
            end
        else
            [contvar_matrix,contvar_info] = std_lm_getcontvars(STUDY,ALLEEG,STUDY.datasetinfo(order(1,s)).subject,'design',design_index,'contvfield',cont_var_fields,'nanmat',1);
            
            % we need to know the nb of trials in each of the sets
            set_positions = [0];
            getnan = find(isnan(contvar_matrix(:,1)));
            for dataset = 1:nb_sets
                cond_size = cellfun(@length,contvar_info.datasetinfo_trialindx(contvar_info.dataset == index(dataset)));
                set_size = sum(cond_size);
                set_positions(dataset+1) = set_size + sum(getnan<set_size);
                % update getnan as the next loop restart at one
                getnan((getnan - set_size)<0) = [];
            end
            set_positions = cumsum(set_positions);
            
            % create contvar updating with set_positions
            contvar = NaN(size(contvar_matrix));
            for cond = 1:size(contvar_info.datasetinfo_trialindx,2)
                which_dataset = contvar_info.dataset(cond); % dataset nb
                dataset_nb = find(which_dataset == order(:,s)); % position in the concatenated data
                position = cell2mat(contvar_info.datasetinfo_trialindx(cond)); % position in the set
                values = contvar_matrix(position+set_positions(dataset_nb)); % covariable values
                contvar(position+set_positions(dataset_nb)) = values; % position in the set + position in the concatenated data
            end
        end
        
        % --> split per condition and zscore
        if exist('categ','var')
            model.cont_files{s} = limo_split_continuous(categ,contvar);
        end
        
        if size(contvar,2) > 1
            save([ALLEEG(index(1)).filepath filesep 'continuous_variables.txt'],'categ','-ascii')
        else
            save([ALLEEG(index(1)).filepath filesep 'continuous_variable.txt'],'categ','-ascii')
        end
    end
end

model.set_files = model.set_files';
model.cat_files = model.cat_files';
if exist('cont_var_fields','var')
    model.cont_files = model.cont_files';
end

% set model.defaults - all conditions no bootstrap
% -----------------------------------------------------------------
% to update passing the timing/frequency from STUDY - when computing measures
% -----------------------------------------------------------------
if strcmp(Analysis,'daterp') || strcmp(Analysis,'icaerp')
    model.defaults.analysis= 'Time';
    model.defaults.start = -10; % ALLEEG(index).xmin*1000;
    model.defaults.end = ALLEEG(index(1)).xmax*1000;
    model.defaults.lowf = [];
    model.defaults.highf = [];
    
elseif strcmp(Analysis,'datspec') || strcmp(Analysis,'icaspec')
    
    model.defaults.analysis= 'Frequency';
    model.defaults.start = -10;
    model.defaults.end = ALLEEG(index(1)).xmax*1000;
    model.defaults.lowf = [];
    model.defaults.highf = [];
    
elseif strcmp(Analysis,'datersp') || strcmp(Analysis,'icaersp')
    model.defaults.analysis= 'Time-Frequency';
    model.defaults.start = [];
    model.defaults.end = [];
    model.defaults.lowf = [];
    model.defaults.highf = [];
end

model.defaults.fullfactorial = 0; % factorial only for single subject analyses - not included for studies
model.defaults.zscore = 0; % done that already
model.defaults.bootstrap = 0 ; % only for single subject analyses - not included for studies
model.defaults.tfce = 0; % only for single subject analyses - not included for studies
model.defaults.method = 'OLS'; % default is OLS - to be updated to 'WLS' once validated
model.defaults.Level= 1; % 1st level analysis
model.defaults.type_of_analysis = 'Mass-univariate'; % future version will allow other techniques

STUDY.design_info = STUDY.design(design_index).variable;
STUDY.design_index = design_index;
STUDY.names = names;

% contrast
if ~isempty(cont_var_fields) && exist('categ','var')
    limocontrast.mat = [zeros(1,max(categ)) ones(1,max(categ)) 0];
    LIMO_files = limo_batch('both',model,limocontrast,STUDY);
else
    limocontrast.mat = [];
    LIMO_files = limo_batch('model specification',model,limocontrast,STUDY);
end
rmfield(STUDY,'design_index');
rmfield(STUDY,'design_info');
rmfield(STUDY,'names');

