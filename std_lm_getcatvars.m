% std_lm_getcatvars() -Retrieve categorical variables from a design in the
% STUDY structure to build the regressors
%
% Usage:
%   >>  [catvar,varnames,StartEndIndx] =
%   std_lm_getcatvars(STUDY,ALLEEG,'S01','design_indx',1);
%
% Inputs:
%      STUDY       - studyset structure containing some or all files in ALLEEG
%      ALLEEG      - vector of loaded EEG datasets
%      subject     - String with the subject identifier
%      design_indx - Index of the design in he STUDY structure
%
% Optional inputs:
%
%
% factors    - Cell array of strings with the name of the conditions per
%              independent variable
%              i.e  factors = {{'IV1_cond1','IV1_cond2','IV1_cond3'}, {'IV2_cond1','IV2_cond2','IV2_cond3'}}
% setindx    - Index of subjects to include in the parsing. This
%              functionality is not designed to be used within the std_lm_setdesign
%              streamline, but for to be used when the function is called as standalone
%              function form command line. By default within the
%              std_lm_setdesign streamline  it will use ALL the subjects
%              under the variable subject
%
%
% Outputs:
% catvar       -  Matrix of integers. Categorical variables retrieved from
%                 the design specified in the STUDY. Each element on each
%                 column will be the index of the condition of that
%                 Independent Variable in cat_regnames.
%                 size(catvar) = Number of trials across all conditions for
%                 that subject x Number of Independent  Variables (2 maximun
%                 if it's comming form EEGLAB study)

% cat_regnames - Cell array with Nb of columns = 3.
%                  1st column: Name of conditions (vectorized for all independent variables)
%                  2nd & 3rd columns: Indices of start and end of sets from the subject in
%                  catvar.
%
% See also:
%   std_lm_setdesign , std_lm_getcontvars
%
% Author: Ramon Martinez-Cancino, SCCN, 2014
%
% Copyright (C) 2014  Ramon Martinez-Cancino,INC, SCCN
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [catvar_matrix,catvar_info] = std_lm_getcatvars(STUDY,ALLEEG,subject,varargin)

% Prevent empty output
catvar_matrix = [];
catvar_info   = [];

%% Varargin stuff
%..........................................................................
try
    options = varargin;
    if ~isempty( varargin ),
        for i = 1:2:numel(options)
            g.(options{i}) = options{i+1};
        end
    else g= []; end;
catch
    error('std_lm_getcatvars() error: calling convention {''key'', value, ... } error'); return;
end;

try, g.design;         catch, g.design      = 1;        end; % By default will be use the fisrt design if not especified
try, g.factors;        catch, g.factors     = '';       end; % If not provided it will use all the factors
try, g.setindx;        catch, g.setindx     = '';       end; % if not provided it will look for all the Sets that belong to this Subject
try, g.nanmat;         catch, g.nanmat      = 0;        end; % If all factors are not provided, it wil fill the catvar_matrix with zeros (1) or NaNs (0)
%% Checking if design


%% Checking setindex and valid subject
%..........................................................................
CurrentSubIndxDesign    = find(strcmp({STUDY.design(g.design).cell.case},subject));

if isempty(subject)
    error('std_lm_getcatvars() error: A valid subject must be provided');
end

CurrentSubIndxDataset = [STUDY.design(g.design).cell(CurrentSubIndxDesign).dataset];

if ~isempty(g.setindx)
    if sum(ismember(CurrentSubIndxDataset, g.setindx)) == 0
        error('std_lm_getcatvars() error: Indices of sets does not match the subject provided ');
    end
    
    for i = 1:length(g.setindx)
        g.setIndxDesign(i) = find(ismember([STUDY.design(g.design).cell.dataset], g.setindx(i)));
    end
    
else
    g.setIndxDesign = CurrentSubIndxDesign;
    g.setindx       = CurrentSubIndxDataset;
end

%% Getting factors
%..........................................................................
if isempty(g.factors)
    g.factors  =  {STUDY.design(g.design).variable.value};
    
    % Cleaning  'g.factors' from empty cells
    for i = 1 : length(g.factors)
        if isempty(g.factors{i}) | strcmp(g.factors{i},''), g.factors(i) = []; end;
    end
else
    % Place to check consitency of input
end

%% Number of trials and index
%..........................................................................
NbTrials   = 0;

TrialIndx_datasetinfo = '';

for i = 1 : length(g.setIndxDesign)
    NbTrials = NbTrials + length(STUDY.design(g.design).cell(g.setIndxDesign(i)).trials{:});
    
    % ---
    if i == 1,
        StartEndIndx{i} = 1: length(STUDY.design(g.design).cell(g.setIndxDesign(i)).trials{:});
    else
        StartEndIndx{i} = StartEndIndx{i-1}(end) +1 : StartEndIndx{i-1}(end) + length(STUDY.design(g.design).cell(g.setIndxDesign(i)).trials{:});
    end
    
    for j = 1 : length(g.factors)
        catvar_names{i,j}   =  STUDY.design(g.design).cell(g.setIndxDesign(i)).value(j);
    end
    
    TrialIndx_datasetinfo{i} =  STUDY.design(g.design).cell(g.setIndxDesign(i)).trials{:};
    dataset{i}               =  STUDY.design(g.design).cell(g.setIndxDesign(i)).dataset;
end

%% Getting categorical variables from STUDY design
%..........................................................................

if g.nanmat
    catvar_matrix = zeros(NbTrials,length(g.factors));                                % Initializing Categorical Variables
else
    catvar_matrix = nan(NbTrials,length(g.factors));
end

% Loop per dataset that belong to subject subj(nsubj)
for nf_dim1 = 1:length(g.factors)                    % per factors
    CondIndx = 1;
    
    for nf_dim2 = 1: length(g.factors{nf_dim1}(:));  % per condition
        
        for i = 1 : length(g.setIndxDesign)          % per dataset
            
            tmp1 = STUDY.design(g.design).cell(g.setIndxDesign(i)).value(nf_dim1);
            if iscell(tmp1)
                tmp1 = tmp1{1};
                if isnumeric(tmp1)
                    tmp1 = num2str(tmp1);
                end
            end
            
            tmp2 = g.factors{nf_dim1}(nf_dim2);
            if iscell(tmp2)
                tmp2 = tmp2{1};
                if isnumeric(tmp2)
                    tmp2 = num2str(tmp2);
                end
            end
            
            
            %             SetCond = find(strcmp(STUDY.design(g.design).cell(g.setIndxDesign(i)).value(nf_dim1),g.factors{nf_dim1}(nf_dim2)));
            SetCond = find(strcmp(tmp1,tmp2));
            if ~isempty(SetCond)
                %                 catvar([StartEndIndx(i,1) : StartEndIndx(i,2)],nf_dim1) = CondIndx;
                catvar_matrix(StartEndIndx{i},nf_dim1) = CondIndx;
            end
        end
        
        CondIndx = CondIndx + 1;
    end
end

%LIMO STUFF
if g.nanmat
    tmp_dat = unique(CurrentSubIndxDataset,'stable');
    
    % getting dataset dimension
    datasets_lengths(1) = 0;
    for i = 1: length(tmp_dat)
%         tmpindx_datasetinfo = STUDY.design(g.design).cell(tmp_dat(i)).dataset;
        tmpindx_ALLEEG      = STUDY.datasetinfo(tmp_dat(i)).index ;
        datasets_lengths(i+1) = ALLEEG(tmpindx_ALLEEG).trials;
    end
    set_positions = cumsum(datasets_lengths);
    
    catvar_nan = nan(set_positions(end),length(g.factors));
    
    start_catvar_indx = 1;
    for i = 1: length(TrialIndx_datasetinfo)
        
        end_catvar_indx = start_catvar_indx + length(TrialIndx_datasetinfo{i})-1;
        
        
        dataset_nb = find(dataset{i} == tmp_dat);
        catvar_nan(TrialIndx_datasetinfo{i} + set_positions(dataset_nb),:) =  catvar_matrix(start_catvar_indx:end_catvar_indx,:);
        
        start_catvar_indx = end_catvar_indx + 1;
    end
    
    % Reasigning result
    catvar_matrix = catvar_nan;
end


catvar_info.datasetinfo_trialindx = TrialIndx_datasetinfo ;
catvar_info.dataset               = [dataset{:}];
catvar_info.designmat_trialindx   = StartEndIndx;
catvar_info.names                 = catvar_names;